const { Observable, ReplaySubject, from, combineLatest, timer, throwError, BehaviorSubject, Subject, empty, merge, Subscriber } = require('rxjs');
const { mergeMap, share, map, defaultIfEmpty, tap, first, filter, retryWhen, switchMap, shareReplay, distinctUntilChanged, bufferToggle, delay, mergeAll, onErrorResumeNext } = require('rxjs/operators');
let debug = require('debug');
let amqp = require('amqplib/callback_api');

let debugConnection = debug('RxRabbit:Connection');
let debugChannel = debug('RxRabbit:Channel');
let debugQueue = debug('RxRabbit:Queue');
let debugExchange = debug(`RxRabbit:Exchange`);
let debugMessage = debug(`RxRabbit:Message`);

/**
 * @typedef {{exchange:string, type:string, options?:amqp.Options.AssertExchange, patterns:string[]}} routingOptions
 */


class RxRabbitConnection {

    /**
     * @param {string} server 
     * @param { {maxRetryAttempts:Number, scalingDuration:number, name:string} }
     */
    constructor(server, { maxRetryAttempts = 5, scalingDuration = 1000, name = undefined } = {}) {
        this.connected = new BehaviorSubject(false);
        let connectionSubject = new BehaviorSubject(this.initConnection(server, { maxRetryAttempts, scalingDuration }));

        this.connection = connectionSubject.pipe(
            switchMap(connection => connection),
            tap(rxConnection => rxConnection.connection.on('close', close => {
                this.connected.next(false);
                if (close.code == 320) {
                    debugConnection('Reconnecting');
                    connectionSubject.next(this.initConnection(server, { maxRetryAttempts, scalingDuration }));
                }
            })),
            shareReplay(1)
        )
        this.server = server;
        this.name = name;
    }

    initConnection(server, { maxRetryAttempts = 5, scalingDuration = 1500 } = {}) {
        return new Observable(observer => {
            debugConnection('Connecting ' + server || '');
            amqp.connect(server, (err, connection) => {
                if (err) {
                    this.connected.next(false);
                    observer.error(err);
                } else {
                    debugConnection('Connected ' + server || '');
                    this.connected.next(true);
                    observer.next(connection);
                    observer.complete();
                }
                return end => {
                    debugConnection('Disconnecting ' + server || '');
                    this.connected.next(false);
                    connection.close();
                }
            })
        })
            .pipe(
                retryWhen(retryStrategy({ maxRetryAttempts, scalingDuration, debug: debugConnection }))
            )
    }

    /**
     * @returns {RxRabbitConfirmChannel}
     */
    createChannel(name) {
        return new RxRabbitChannel(this, name)
    }

    /**
     * @returns {RxRabbitConfirmChannel}
     */
    createConfirmChannel(name) {
        return new RxRabbitConfirmChannel(this, name)
    }
}

class RxRabbitQueue {
    /**
     * 
     * @param { RxRabbitConfirmChannel } channel
     * @param { string } name
     * @param {amqp.Options.AssertQueue} options
     */
    constructor(channel, name) {
        this.name = name;
        this.channel = channel;
    }

    /**
     * 
     * @param {amqp.Options.Consume} options 
     * @returns {Observable<RxRabbitMessage>}
     */
    consume(options) {
        return this.channel.consume(this.name, options);
    }

    /**
     * 
     * @param {string} pattern 
     * @param {string} source 
     * @returns {RxRabbitQueue}
     */
    addTopic(exchange, pattern) {
        return this.channel.bindQueue(this.name, exchange, pattern).pipe(map(result => this, this))
    }

    /**
     * 
     * @param {string} pattern 
     * @param {string} exchange 
     * @returns {Observable<RxRabbitQueue>}
     */
    bindExchange(exchange, pattern = '') {
        return this.channel.bindQueue(this.name, exchange, pattern).pipe(map(result => this, this))
    }
}

class RxRabbitMessage {
    /**
     * 
     * @param {RxRabbitConfirmChannel} channel 
     * @param {amqp.Message} message 
     */
    constructor(channel, message) {
        this.channel = channel;
        this.raw = message;
        this.properties = message.properties;
        this.content = message.content;
        this.fields = message.fields;
    }

    ack() {
        try{
            this.channel.ack(this.raw);
        } catch(e){
            if(e.message !== 'Channel closed'){
                throw(e)
            }
        }
    }

    nack() {
        try{
            this.channel.nack(this.raw);
        } catch(e){
            if(e.message !== 'Channel closed'){
                throw(e)
            }
        }
    }
}

class RxRabbitSendMessage {
    /**
     * 
     * @param { {exchange:string, routingKey:string, content:Buffer, options:amqp.Options.Publish} }
     * @param { Subscriber<amqp.Message> } subscriber 
     */
    constructor({exchange, routingKey, content, options}, subscriber) {
        
        this.isBuffered = false;
        this.content = content;
        this.exchange = exchange;
        this.routingKey = routingKey;
        this.options = options;

        /**
         * @param {Observable<amqp.Channel>} channel
         * @return {Observable<amqp.Message>}
         */
        this.send = channel => channel.pipe(
            switchMap(channel => new Observable(
                observer => {
                    let result = channel.publish(exchange, routingKey, content, Object.assign(options), (err, ok) => {
                        if (err) {
                            observer.error(err);
                        } else {
                            observer.next({
                                content: content,
                                properties: options,
                                fields: {
                                    exchange: exchange,
                                    routingKey: routingKey,
                                    result: result
                                }
                            })
                            observer.complete();
                        }
                    })
                }
            )),
            tap(subscriber)
        )
    }

    /**
     * 
     * @param {boolean} buffered 
     * @returns {RxRabbitSendMessage}
     */
    buffered(buffered){
        this.isBuffered = buffered;
        return this;
    }
}

class RxRabbitChannel {

    /**
     * 
     * @param {RxRabbitConnection} connection 
     */
    constructor(connection, name, prefetch = 10) {
        /**
         * @type {Subject<RxRabbitSendMessage>}
         */
        this.messages = new Subject();
        this.name = name;
        this.connection = connection;
        this.connected = new BehaviorSubject(false);


        /**
         * @type {Observable<amqp.Channel>}
         */
        this.channel = this.connection.connection.pipe(
            switchMap(connection => this.initChannel(connection, name)),
            tap(() => this.connected.next(true)), // set connected when channel is created
            tap(channel => channel.prefetch(prefetch)),
            shareReplay(1),
        )

        this.connection.connected.pipe(distinctUntilChanged(), filter(connected => !connected)).subscribe(this.connected);

        let buffer = this.messages
            .pipe(
                bufferToggle(this.connected.pipe(filter(connected => !connected), tap(connected => debugMessage('Diverting messages to buffer'))), () => this.connected.pipe(filter(connected => connected), tap(connected => debugMessage('Sending messages live')))),
                tap((messages)=>debugMessage(`Buffer drain - ${messages.length} items`)),
                delay(500),
                mergeAll(),
                map(message => message.buffered(true))
            )

        merge(this.connected.pipe(switchMap(connected => connected ? this.messages : empty())), buffer)
            .pipe(
                tap(message => debugMessage(`Sending ${message.isBuffered ? ' buffered ' : ' live '} message`)),
                switchMap(message => message.send(this.channel)),
                onErrorResumeNext()
            )
            .subscribe(
                message => debugMessage(`Sent`),
                message => console.error(message)
            )

    }


    /**
     * 
     * @param {amqp.Connetion} connection 
     * @param {string} name 
     * @returns {Observable<amqp.Channel>}
     */
    initChannel(connection, name = null) {
        return new Observable(observer => {
            debugChannel('Creating ' + name || '');
            connection.createChannel((err, channel) => {
                if (err) {
                    observer.error(err)
                    debugChannel('Error', err);
                } else {
                    observer.next(channel);
                    debugChannel('Created ' + name || '');
                }
                observer.complete();

                channel.on('error', (error) => {
                    debugChannel('Error', err);
                    observer.error(error);
                })
                return end => {
                    channel.close();
                    debugChannel('Closed ' + name || '');
                }
            })
        })
    }

    /**
     * 
     * @param {amqp.Message} message 
     */
    ack(message) {
        this.channel.subscribe(channel => channel.ack(message))
    }

    /**
     * 
     * @param {amqp.Message} message 
     */
    nack(message) {
        this.channel.subscribe(channel => channel.nack(message))
    }

    /**
     * 
     * @param {string} queue 
     * @param {Buffer} content 
     * @param {amqp.Options.Publish} options 
     */
    sendToQueue(queue, content, options) {
        return this.channel.pipe(
            switchMap(channel => new Observable(
                observer => {
                    this.channel.sendToQueue(queue, content, options)
                    observer.next({
                        content: content,
                        properties: options,
                        fields: {
                            // exchange: exchange,
                            // routingKey: routingKey
                        }
                    })
                    observer.complete();
                }
            )))
    }

    /**
     * 
     * @param {string} exchange 
     * @param {string} routingKey 
     * @param {Buffer} content 
     * @param {amqp.Options.Publish} options 
     * @returns {Observable<amqp.Message>}
     */
    publish(exchange, routingKey, content, options) {
        this.messages.next({ exchange, routingKey, content, options, buffered: false });
        return empty();
        // return this.channel.pipe(
        //     switchMap(channel => new Observable(
        //         observer => {
        //             channel.publish(exchange, routingKey, content, options)
        //             observer.next({
        //                 content: content,
        //                 properties: options,
        //                 fields: {
        //                     exchange: exchange,
        //                     routingKey: routingKey
        //                 }
        //             })
        //             observer.complete();
        //         }
        //     ))
        // )
    }

    /**
     * 
     * @param {string} exchange 
     * @param {string} type 
     * @param {amqp.Options.AssertExchange} options 
     * @returns {Observable<RxRabbitExchange>}
     */
    assertExchange(exchange, type, options, channel) {
        return this.channel.pipe(
            mergeMap(channel => new Observable(observer => {
                debugExchange('Creating ' + exchange);
                channel.assertExchange(exchange, type, options, (err, reply) => {
                    if (err) {
                        observer.error(err);
                    } else {
                        debugExchange('Created ' + exchange);
                        observer.next(new RxRabbitExchange(this, exchange, options));
                    }
                    observer.complete();
                });
            })
            ),
            first(),    // this is an intentional bug, need to move routing out of rabbitQueue function
            shareReplay(1)
        )

    }

    /**
     * 
     * @param {string} name 
     * @param {amqp.Options.AssertQueue} options 
     * @param {routingOptions[]} routing
     * @returns {Observable<RxRabbitQueue>}
     */
    assertQueue(name, options, routing = []) {
        return this.channel.pipe(
            mergeMap(channel => new Observable(observer => {
                channel.assertQueue(name, options, (err, reply) => {
                    if (err) {
                        observer.error(err);
                        debugQueue('Creating ' + name);
                    } else {
                        debugQueue(`Created ${reply.queue}, ${reply.messageCount} unread messages, ${reply.consumerCount} consumers`);
                        observer.next(new RxRabbitQueue(this, reply.queue, options));
                    }
                    observer.complete();
                });
            })),
            mergeMap(queue =>
                from(routing)
                    .pipe(
                        switchMap((route) => this.assertExchange(route.exchange, route.type, route.options || {})
                            .pipe(
                                mergeMap(result => route.patterns),
                                mergeMap(pattern => queue.bindExchange(route.exchange, pattern)),
                                map(() => queue)
                            )
                        ),
                        defaultIfEmpty(queue)
                    )
            ),
            shareReplay(1)
        )
    }

    /**
     * 
     * @param {string} queue 
     * @param {amqp.Options.Consume} options 
     * @returns {Observable<RxRabbitMessage>}
     */
    consume(queue, options) {
        return this.channel.pipe(
            switchMap(channel => new Observable(
                observer => {
                    channel.consume(queue, message => observer.next(new RxRabbitMessage(channel, message)), options)
                }
            )),
            tap(() => debugMessage('Recieved')),
            shareReplay(1)
        )
    }

    /**
     * 
     * @param {string} queue 
     * @param {string} exchange 
     * @param {string} pattern 
     * @param {any} options
     * @returns {Observable<RxRabbitChannel>}
     */
    bindQueue(queue, exchange, pattern = '', options = null) {
        return this.channel.pipe(
            switchMap(channel => new Observable(
                observer => {
                    channel.bindQueue(queue, exchange, pattern, options, (err, reply) => {
                        if (err) {
                            observer.error(err);
                            observer.complete();
                        } else {
                            observer.next(this);
                        }
                    })
                    return () => observer.complete();
                }
            ))
        )
    }
}

/**
 * @property {Observable<amqp.ConfirmChannel>} channel
 */
class RxRabbitConfirmChannel extends RxRabbitChannel {

    /**
     * 
     * @param {Observable<amqp.Connection>} connection 
     */
    constructor(connection, name) {
        super(connection, name);
    }

    initChannel(connection, name) {
        return new Observable(observer => {
            debugChannel('Creating' + this.name || '');
            connection.createConfirmChannel((err, channel) => {
                if (err) {
                    observer.error(err)
                    debugChannel('Error', err);
                } else {
                    observer.next(channel);
                    observer.complete();

                    debugChannel('Created' + this.name || '');
                }
                channel.on('error', (error) => {
                    debugChannel('Error', error);
                    observer.error(error);
                })
                return end => {
                    debugChannel('Closed' + this.name || '');
                    channel.close();
                }
            })
        })
    }

    /**
     * 
     * @param {string} queue 
     * @param {Buffer} content 
     * @param {amqp.Options.Publish} options 
     * @returns {Observable<amqp.Replies.Empty>}
     */
    sendToQueue(queue, content, options) {
        options.mandatory = true;
        return this.channel.pipe(
            switchMap(channel => new Observable(
                observer => {
                    channel.sendToQueue(queue, content, options, (err, ok) => {
                        if (err) {
                            observer.error(err);
                        } else {
                            observer.next({
                                content: content,
                                properties: options,
                                fields: {
                                    exchange: exchange,
                                    routingKey: routingKey
                                }
                            })
                        }
                        observer.complete();
                        return () => observer.complete();
                    })
                }
            )))
    }

    /**
     * 
     * @param {string} exchange 
     * @param {string} routingKey 
     * @param {Buffer} content 
     * @param {amqp.Options.Publish} options
     * @returns {Observable<amqp.Message>}>}
     */
    publish(exchange, routingKey, content, options = {}) {
        return new Observable(
            observer => {
                this.messages.next(new RxRabbitSendMessage({exchange, routingKey, content, options},observer));
            }
        )
    }
}

class RxRabbitExchange {
    /**
     * 
     * @param {RxRabbitConfirmChannel} channel 
     * @param {string} name 
     * @param {amqp.Options.AssertExchange} options 
     */
    constructor(channel, name, options) {
        this.name = name;
        this.channel = channel;
        this.options = options;
    }

    /**
     * 
     * @param {string} queue 
     * @param {string} pattern 
     */
    bindQueue(queue, pattern) {
        return this.channel.bindQueue(queue, this.name, pattern).pipe(map(() => this, this));
    }

    /**
     * 
     * @param {string} routingKey 
     * @param {Buffer} content 
     * @param {amqp.Options.Publish} options 
     * @returns {Observable<amqp.Message>}
     */
    publish(routingKey, content, options = {}) {
        return this.channel.publish(this.name, routingKey, content, options)
    }
}

class RxRabbitTopic {
    constructor(channel, key, exchange) {
        this.key = key;
        this.channel = channel;
        this.exchange = exchange;
    }

}


/**
 * @type {ReplaySubject<string>}
 * @private
 */
const _connectionOptions = new ReplaySubject(1);

/**
 * @type {ReplaySubject<string>}
 * @private
 */
const _queueOptions = new ReplaySubject(1);

/**
 * @type {ReplaySubject<RxRabbitConnection>}
 * @private
 */
const _connection = new ReplaySubject(1);

/**
 * @type {ReplaySubject<RxRabbitQueue>}
 * @private
 */
const _queue = new ReplaySubject(1);

/**
 * @type {Observable<RxRabbitConfirmChannel>}
 * @private
 */
const _channel = _connectionOptions
    .pipe(
        first(),
        map(options => new RxRabbitConnection(options.server, options)),
        tap(_connection),
        map(connection => connection.createConfirmChannel('default')),
        shareReplay(1) // reuse this default channel!!
    )

/**
 * @type {ReplaySubject<amqp.Options.Consume>}
 * @private
 */
const _consumeOptions = new ReplaySubject(1);

/**
 * @type {Observable<RxRabbitMessage>}
 * @private
 */
const _consume = combineLatest(
    _queue.pipe(first()),
    _consumeOptions.pipe(
        first()
    )
)
    .pipe(
        switchMap(([queue, options]) => queue.consume(Object.assign({ consumerTag: queue.name }, options))),
        shareReplay(1)
    )

/**
 * @class
 * @public
 */
class RxRabbit {

    constructor() {
        this.connection = this.createConnection()
    }

    get Connection() { return _connection.asObservable() }
    get Queue() { return _queue.asObservable() }
    /**
     * @type {Observable<RxRabbitConfirmChannel>}
     */
    get Channel() { return _channel }

    /**
     * @typedef Options
     * @property { amqp.Options.AssertQueue } queue
     * @property { amqp.Options.Consume } consume 
     * @property { {maxRetryAttempts:number, scalingDuration:number} } connection 
     */


    /**
     * 
     * @param {string} server 
     * @param { Options } options 
     * @param {routingOptions[]} routing 
     * @returns {Observable<RxRabbitConnection>}
     */
    connect(server, options = {}, queueName = undefined, routing = []) {
        _connectionOptions.next(Object.assign({server, name:'default'},options.connection || {}));
        _consumeOptions.next(options.consume);
        _queueOptions.next({ queueName: queueName, options: options.queue, routing: routing })

        _channel.pipe(mergeMap(channel => channel.assertQueue(queueName || '', options.queue || {}, routing || [])), first(), shareReplay(1)).subscribe(_queue);
        return _connection;
    }

    /**
     * @private
     * @param {string} server 
     * @param { {maxRetryAttempts:Number, scalingDuration:number, name:string} }
     * @returns {RxRabbitConnection}
     */
    createConnection(server, { maxRetryAttempts = 6, scalingDuration = 2000, name = undefined } = {}) {
        return new RxRabbitConnection(server, { maxRetryAttempts, scalingDuration, name });
    }

    /**
     * 
     * @param {string} name
     * @param {amqp.Options.AssertQueue} options
     * @param {routingOptions[]} routing
     * @returns {Observable<RxRabbitQueue>}
     */
    assertQueue(name, options, routing = []) {
        return this.Channel.pipe(
            mergeMap(channel => channel.assertQueue(name, options, routing)),
            share()
        )
    }

    /**
     * 
     * @param {string} exchange 
     * @param {string} type 
     * @param {amqp.Options.AssertExchange} options 
     * @returns {Observable<RxRabbitExchange>}
     */
    assertExchange(exchange, type, options) {
        return this.Channel.pipe(
            mergeMap(channel => channel.assertExchange(exchange, type, options))
        )
    }

    /**
     * 
     * @typedef Filter 
     * @property { string } routingKey
     * @property { string } type
     */

    /**
     * 
     * @param { Filter }  filter
     * @param {Observable<RxRabbitMessage>}
     */
    consume(filters = {}) {

        if (filters.routingKey || filters.type) {
            return _consume.pipe(
                filter(message => (filters.type == undefined && message.fields.routingKey == filters.routingKey) || (filters.routingKey == undefined && message.properties.type == filters.type) || (message.properties.type == filters.type && message.fields.routingKey == filters.routingKey))
            )
        }
        return _consume.pipe(shareReplay(1));
    }

    /**
     * 
     * @param {string} queue 
     * @param {Buffer}
     * @param {amqp.Options.Publish} options 
     */
    sendToQueue(queue, content, options) {
        return this.Channel.pipe(mergeMap(channel => channel.sendToQueue(queue, content, options)))
    }

    bindQueue(exchange, pattern = '') {
        return this.Queue.pipe(map(queue => queue.bindQueue(exchange, pattern)));
    }


    /**
     * 
     * @param {string} exchange 
     * @param {string} routingKey 
     * @param {Buffer} content 
     * @param {amqp.Options.Publish} options 
     */
    publish(exchange, routingKey, content, options) {
        return _channel.pipe(mergeMap(channel => channel.publish(exchange, routingKey, content, options)))
    }
}

/**
  * 
  * @param { {maxRetryAttempts:Number, scalingDuration:number, debug:(text:string)=>{}} }
  * @return {function(): Observable<number>}
  */
function retryStrategy({ maxRetryAttempts = 5, scalingDuration = 1000, debug = (text) => { } } = {}) {

    /**
     * 
     * @param {Observable<any>} attempts 
     * @returns {Observable<number>}
     */
    let retry = function (attempts) {
        return attempts.pipe(
            mergeMap((error, i) => {
                const retryAttempt = i + 1;
                if (retryAttempt > maxRetryAttempts) {
                    return throwError(error);
                } else {
                    debug(
                        `Attempt ${retryAttempt}: retrying in ${retryAttempt *
                        scalingDuration}ms`
                    );
                    return timer(retryAttempt * scalingDuration)
                }
            })
        )
    }
    return retry;
}

module.exports = RxRabbit;