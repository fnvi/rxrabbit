const {Observable} = require('rxjs');
let amqp = require('amqplib/callback_api');
let RxRabbitChannel = require('./channel')

class RxRabbitExchange {
    /**
     * 
     * @param {RxRabbitChannel} channel 
     * @param {string} name 
     * @param {amqp.Options.AssertExchange} options 
     */
    constructor(channel,name, options){
        this.name = name;
        this.channel = channel;
        this.options = options;
    }

    /**
     * 
     * @param {string} routingKey 
     * @param {Buffer} content 
     * @param {amqp.Options.Publish} options 
     */
    publish(routingKey, content, options){
        return this.channel.publish(this.name,routingKey, content, options)
    }
}