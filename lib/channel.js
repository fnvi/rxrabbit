const {Observable} = require('rxjs');
let amqp = require('amqplib/callback_api');

class RxRabbitChannel {

    /**
     * 
     * @param {amqp.Channel} channel 
     */
    constructor(channel){
        this.channel = channel
    }

    /**
     * 
     * @param {string} queue 
     * @param {Buffer} content 
     * @param {amqp.Options.Publish} options 
     */
    sendToQueue(queue, content, options){
        return this.channel.sendToQueue(queue, content, options)
    }

    /**
     * 
     * @param {string} exchange 
     * @param {string} routingKey 
     * @param {Buffer} content 
     * @param {amqp.Options.Publish} options 
     */
    publish(exchange, routingKey, content, options){
        return this.channel.publish(exchange, routingKey, content, options)
    }

    assertExchange(exchange, type, options){
        return new Observable(observer => {
            this.channel.assertExchange(exchange, type, options, (err, reply) => {
                if (err) {
                    observer.error(err)
                    observer.complete()
                }
                console.log(reply);
                observer.next(new RxRabbitExchange(this,exchange,options));
            });
            return () => console.log('Queue closed ', name || 'exclusive');
        })
    }

    /**
     * 
     * @param {string} name 
     * @param {amqp.Options.AssertQueue} options 
     * @returns {Observable<RxRabbitQueue>}
     */
    assertQueue(name, options){
        return new Observable(observer => {
            this.channel.assertQueue(name, options, (err, reply) => {
                if (err) {
                    observer.error(err)
                    observer.complete()
                }
                console.log(reply);
                observer.next(new RxRabbitQueue(this,name, options));
            });
            return () => console.log('Queue closed ', name || 'exclusive');
        })
    }

    consume(queue, options){
        return new Observable(
            observer => {
                this.channel.consume(queue,message => observer.next(message),options)
            }
        )
    }
}

module.exports = RxRabbitChannel;