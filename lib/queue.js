const {Observable} = require('rxjs');
let amqp = require('amqplib/callback_api');
let RxRabbitChannel = require('./channel')

class RxRabbitQueue {
    /**
     * 
     * @param {RxRabbitChannel} channel
     * @param {string} name
     * @param {amqp.Options.AssertQueue} options
     */
    constructor(channel, name) {
        this.name = name;
        this.channel = channel;
    }

    /**
     * 
     * @param {string | Buffer} message 
     * @param {amqp.Options.Publish} options
     */
    send(message, options) {
        return this.channel.sendToQueue(this.name,typeof message == 'Buffer' ? message : Buffer.from(message,'utf8'),options)
    }

    /**
     * 
     * @param {amqp.Options.Consume} options 
     * @returns {Observable<amqp.Message>}
     */
    consume(options){
        return this.channel.consume(this.name, options)
    }

}

module.exports = RxRabbitQueue;