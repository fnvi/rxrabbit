const {Observable} = require('rxjs');
let amqp = require('amqplib/callback_api');
let RxRabbitChannel = require('./channel')

class RxRabbitTopic {
    constructor(channel, key, exchange){
        this.key = key;
        this.channel = channel;
        this.exchange = exchange;
    }

}