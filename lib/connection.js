const {Observable} = require('rxjs');
let amqp = require('amqplib/callback_api');
let RxRabbitChannel = require('./channel');

class RxRabbitConnection {

    /**
     * 
     * @param {amqp.Connection} connection 
     */
    constructor(connection, name) {
        this.connection = connection;
        this.name = name;
    }

    /**
     * @returns {Observable<RxRabbitChannel>}
     */
    createChannel(name) {
        return new Observable(observer => {
            debug('RxRabbit:Channel')('Creating' + name || '');
            this.connection.createChannel((err, channel) => {
                if (err) {
                    observer.error(err)
                    debug('RxRabbit:Error')('Error', err);
                } else {
                    observer.next(new RxRabbitChannel(channel, name));
                    debug('RxRabbit:Channel')('Created' + name || '');
                }
                observer.complete();
                return end => {
                    channel.close();
                    debug('RxRabbit:Channel')('Closed' + name || '');
                }
            })
        })
    }
}

module.exports = RxRabbitConnection;