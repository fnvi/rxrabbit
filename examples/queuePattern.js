const { throwError, timer, of, Observable, ReplaySubject, Subject, BehaviorSubject, merge, forkJoin, empty} = require('rxjs');
const {retryWhen, mergeMap, finalize, delay,tap,filter, switchMap, map,concatMap, catchError, onErrorResumeNext, bufferToggle, mergeAll, publish,ignoreElements } = require('rxjs/operators');


let messages$ = timer(0,500) // send message every 0.5 seconds
.pipe(
    map(val => `Message ${val}`)
)



let connection$ = timer(500,2000) // disconnects and reconnectes every 2 seconds
.pipe(
    map(val => val % 2 == 0),
    mergeMap(connected => connected ? of('connected') : throwError('disconnected')),
)

let disconnected$ = connection$.pipe(ignoreElements(),catchError(err => 'something')); // observes errors only
let connected$ = connection$.pipe(filter(connection => connection == true)); // observes connections

let queue$ = messages$
.pipe(
   bufferToggle(disconnected$,(test)=>{console.log(test); return connected$}),
   mergeAll()
)

merge(connection$.pipe(switchMap(connected=>connected ? messages$ : empty())),queue$)
.subscribe(
    message => console.log(message),
    message => console.log(message)
)

function createConnection(){
    return new Observable(
        observer => {
            observer.next()
        }
    )
}
