const { of, merge } = require('rxjs');
const { map, mergeMap, delay, tap, filter } = require('rxjs/operators');
process.env.DEBUG = 'RxRabbit:*'
let RxRabbit = require('../index');

var messages = 0;

RxRabbit.consume({routingKey:'item.modified.even'})
.pipe(

    mergeMap(message => handleMessage(message))
)
.subscribe(
    result => console.log('even',result),
    error => console.log('error',error)
)

RxRabbit.consume({routingKey:'item.modified.odd'})
.pipe(
    delay(5000),
    mergeMap(message => handleMessage(message)) 
)
.subscribe(
    result => console.log('odd',result),
    error => console.log('error',error)
)

/**
 * 
 * @param {RxRabbitMessage} message 
 */
function handleMessage(message){
    let original = message;
    return of(message)
    .pipe(
        tap(() => console.log('holding '+ (++messages) + 'messages')),
        delay(5000),
        map(message => message.content.toString('utf8')),
        tap(() => original.ack(), () => original.nack()),
        tap(() => console.log('holding '+ (--messages) + 'messages')),

    )
}

RxRabbit
.connect('amqp:localhost',{consume:{consumerTag:'service name'},queue:{durable:true},connection:{scalingDuration:3000}},'queue2',[{type:'topic',exchange:'fnvi',options:{durable:true},patterns:['item.modified.*']}])
.subscribe(
    connection => console.log('connected'),
    err => console.log(err),
    () => console.log('end')
)