const { combineLatest, timer } = require('rxjs');
const { map, switchMap } = require('rxjs/operators')
process.env.DEBUG = '*';
let RxRabbit = require('../index');



combineLatest(timer(500,1000),RxRabbit.assertExchange('fnvi','topic',{durable:false}))
.pipe(
    map(([timer, exchange])=> exchange.publish('item.created',Buffer.from(`Item Created ${timer}`))),
    map(buffer => buffer.toString('utf8'))
)
.subscribe(
    message => console.log(`sent ${message}`),
    error => console.log('error',error),
    () => console.log('ended')
)


RxRabbit.connect('amqp://localhost').subscribe(result => console.log('result'),err=>console.error(err))