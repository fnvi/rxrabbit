const { throwError, timer, of, Observable, ReplaySubject} = require('rxjs');
const {retryWhen, mergeMap, finalize, delay,tap} = require('rxjs/operators');
let events = require('events');

let connectSubject = new ReplaySubject(1);
let connectionSubject = new ReplaySubject(1);

connectSubject
.pipe(
    mergeMap(details => createConnection(details)),
    tap(connection => connection.on('close',err => {
        console.log('disconnected, reconnecting');
        connectSubject.next({successfulAttempt:2});
    }))
)
.subscribe(connectionSubject)


function createConnection(options){
    var currentAttempt = 0;
    return new Observable(
        observer => {
            let connector = new events.EventEmitter();
            connector.once('connected',connection => {
                // process.stdin.resume();
                observer.next(connection);
                observer.complete();
            })
            connector.on('error',error => observer.error(error));
            if(options.successfulAttempt == currentAttempt){
                let _connection = new events.EventEmitter();
                connector.emit('connected',_connection);
            } else {
                currentAttempt++;
                connector.emit('error',{code:1, message:'connection error'})
            }
        }
    )
    .pipe(
        retryWhen(retryStrategy())
    )
}

 /**
  * 
  * @param { {maxRetryAttempts:Number, scalingDuration:number} }
  * @return {function(): Observable<number>}
  */
function retryStrategy({maxRetryAttempts=5,scalingDuration=1000}={}){

    /**
     * 
     * @param {Observable<any>} attempts 
     * @returns {Observable<number>}
     */
    let retry = function(attempts){
        return attempts.pipe(
            mergeMap((error, i)=>{
                const retryAttempt = i+1;
                if(retryAttempt > maxRetryAttempts){
                    return throwError(error);
                } else {
                    console.log(
                        `Attempt ${retryAttempt}: retrying in ${retryAttempt *
                          scalingDuration}ms`
                      );
                    return timer(retryAttempt * scalingDuration)
                }
            }),
            finalize(() => console.log('connected'))
        )
    }
    return retry;
}

function connect(options){
    connectSubject.next(options);
    return connectionSubject
}

connect({successfulAttempt:3}).pipe(
    delay(2000),
    tap(connection => connection.emit('close',{code:2,message:'Disconnected'}))
)
.subscribe(
    result =>{},
    error => console.log('error',error)
)