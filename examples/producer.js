const { combineLatest, timer, Subject} = require('rxjs');
const { map, concatMap, mergeMap, tap , switchMap} = require('rxjs/operators')
process.env.DEBUG = 'RxRabbit:*'
let RxRabbit = require('../index');

let output = new Subject();
output.pipe(
    mergeMap(timer=> RxRabbit.publish('fnvi',`item.modified.${timer % 2 == 0 ? 'even': 'odd'}`,Buffer.from(`Item Modified ${timer}`),{persistent:true})),
)
.subscribe(
    message => console.log(`sent ${message.content.toString('utf8')}`),
    error => console.log('error',error),
    () => console.log('ended')
)

timer(500,200).subscribe(output);


RxRabbit.connect('amqp://localhost',{queue:{exclusive:true, autoDelete:true}}).subscribe()