const { combineLatest, timer } = require('rxjs');

const { mergeMap, map } = require('rxjs/operators');
let RxRabbit = require('../index');

let exchange = RxRabbit.createConnection('amqp:localhost').pipe(
    mergeMap(connection => connection.createChannel()),
    mergeMap(channel => channel.assertExchange('fnvi','topic',{durable:false}))
)

RxRabbit.consume({noAck:true})
// .pipe(
//     map(message => message.content.toString('utf8'))
// )
.subscribe(
    result => console.log(result),
    error => console.log('error',error),
    () => console.log('finished')
)

setTimeout(()=>{
    console.log('sending message');
    RxRabbit.publish('fnvi','item.modified',Buffer.from('hello'),{timestamp:Date.now(),contentType:'binary',messageId:'1',type:'something'}).subscribe();
},2000)

// combineLatest(timer(500,1000),exchange)
// .pipe(
//     map(([timer, exchange])=> exchange.publish('item.modified',Buffer.from(`Item Modified2 ${timer}`),{timestamp:Date.now(), type:'some schema', contentType:'avro/binary', messageId:})),
//     map(buffer => buffer.toString('utf8'))
// )
// .subscribe(
//     message => console.log(`sent ${message}`),
//     error => console.log('error',error),
//     () => console.log('ended')
// )

RxRabbit
.connect('amqp:localhost',{durable:false},'queue2',[{type:'topic',exchange:'fnvi',options:{durable:false},patterns:['item.*']}])
.subscribe(
    a => console.log('a',a),
    a => console.log('b',a),
    () => console.log('done')
)